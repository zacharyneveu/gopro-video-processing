#! /bin/sh
#
# split-segments.sh
# Copyright (C) 2024 zach <zach@Zach-Desktop>
#
# Distributed under terms of the MIT license.
#


ffmpeg.exe -i "$1" -c copy -map 0 -segment_time 00:10:00 -f segment -reset_timestamps 1 "splits/${1%.mp4}-%03d.mp4"
