#! /bin/sh
#
# merge-and-export-gps.sh
# Copyright (C) 2023 zach <zach@Zach-Desktop>
#
# Distributed under terms of the MIT license.
#

cwd=$(pwd)
cd "$1"

echo -n '' > merge.txt
# Create list of files to merge - gopro files sorted by name should also be in date order
# Leave `merge.txt` around just in case
for file in *.MP4; do
	echo "file ${file}" >> merge.txt
done

# Merge videos
ffmpeg.exe -f concat -i merge.txt -c copy -map 0:v -map 0:a -map 0:3 -copy_unknown -tag:2 gpmd combined.mp4

# Create combined GPS track
gopro2gpx.exe combined.mp4

cd "${cwd}"
